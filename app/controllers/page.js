const pages = require('./../../pages.json').pages;

exports.getPage = (req, res) => {
    const alias = req.params.alias;
    const lang = req.cookies.lang == undefined ? 'ru' : req.cookies.lang;

    try {
        const title = pages[alias]['title_' + lang];
        
        let nextPage;
        if (pages[alias].next != 'none') {
            nextPage = {
                title: pages[pages[alias].next]['title_' + lang],
                description: pages[pages[alias].next]['description_' + lang],
                img: pages[pages[alias].next].image,
                url: '/' + pages[alias].next
            }
        }

        let prevPage;
        if (pages[alias].prev != 'none') {
            prevPage = {
                title: pages[pages[alias].prev]['title_' + lang],
                description: pages[pages[alias].prev]['description_' + lang],
                img: pages[pages[alias].prev].image,
                url: '/' + pages[alias].prev
            }
        }
        res.render(`${lang}/${alias}.html`, {title, alias, nextPage, prevPage});
    } catch (err) {
        // console.log(err);
        res.send('<h1>404 Page not found</h1>');
    }
}

exports.swapLang =(req,res) =>{
    let lang = req.cookies.lang == 'ru' || req.cookies.lang == undefined ? 'en' :'ru';
    res.cookie('lang',lang);
    res.redirect('back');

}