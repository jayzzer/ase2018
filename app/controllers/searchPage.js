const pages = require('../../pages.json').pages;
const convert = require('../utils/convertHtmlToText');
const fs = require('fs');

exports.getSearchPage = (req, res) => {
    const lang = req.cookies.lang == undefined ? 'ru' : req.cookies.lang;
    const pageTitle = pages['search_page']['title_' + lang];
    const userReq = req.params.userReq.trim().toLowerCase();

    let badsearch = '';
    if (!userReq) {
        if (lang == 'ru') {
            badsearch = 'Пустой поисковый запрос';
        } else {
            badsearch = 'Empty search query';
        }

        res.render(`${lang}/search_page.html`, { 
            title: pageTitle,
            badsearch: badsearch 
        });
        
        return;
    }

    const pageNames = Object.keys(pages);
    let foundPages = {}; 

    pageNames.forEach( page => {
        const fileContent = fs.readFileSync(`src/view/${lang}/${page}.html`, "utf8"); //считываем файл
        const clearString = convert.convertHtmlToText(fileContent); //удаляем тэги
        const lowerCaseText = clearString.toLowerCase();

        const foundWordIndex = lowerCaseText.indexOf(userReq);
        if (foundWordIndex + 1) {
            const context = convert.getContext(clearString, userReq, foundWordIndex);
            const pageInf = pages[page];

            foundPages[page] = {
                name: pageInf['title_' + lang],
                context
            };
        }
    } );
    
    const foundPagesCount = Object.keys(foundPages).length;
    if (!foundPagesCount) {
        if (lang == 'ru') {
            badsearch = `По запросу: "${userReq}" ничего не найдено`;
        } else {
            badsearch = `Nothing found for "${userReq}"`;
        }

        res.render(`${lang}/search_page.html`, { 
            title: pageTitle,
            badsearch: badsearch 
        });
    } else {
        let goodsearch;
        if (lang == 'ru') {
            goodsearch = `По запросу "${userReq}" найдено ${foundPagesCount} совпадений:`;
        }
        else {
            goodsearch = `${foundPagesCount} matches found on request "${userReq}"`;
        }

        res.render(`${lang}/search_page.html`, {
            title: pageTitle,
            search: foundPages,
            goodsearch: goodsearch
        });
    }
}


exports.searching = (req, res) => {
    let userReq = req.body.userReq;
    res.redirect(`/search/${userReq}`);
}