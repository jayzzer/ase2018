const config = require('../../config');
const email = require('emailjs/email');

const server = email.server.connect({
    user: 'jayzzer@yandex.ru',
    password: config.emailPass,
    host: 'smtp.yandex.ru',
    ssl: true
});

/**
 * Отправка письма на почту
 * @param {Object} message 
 */
exports.sendEmail = message => {
    server.send(message, function(err, message) { console.log(err); });
} 