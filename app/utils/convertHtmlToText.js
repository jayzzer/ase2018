var exports = module.exports = {};

exports.getContext = function (inputText, word, wordIndex) {
    inputText = inputText.replace(new RegExp(word,'gi'), '<strong>' + word + '</strong>');
    return '...' + inputText.substring(wordIndex - 200, wordIndex + word.length + 200) + '...';
}

exports.convertHtmlToText = function (inputText) {
    var returnText = inputText.substring(inputText.indexOf('content %}') + 10);

    returnText = returnText.replace(/<head.*?>[\w\W]*<\/head>/gi, "");
    returnText = returnText.replace(/<button.*?>[\w\W]*?<\/button>/gi, "");
    returnText = returnText.replace(/\{\{[\w\W]*?\}\}/gi, "");
    returnText = returnText.replace(/\{%[\w\W]*?%\}/gi, "");
    //-- remove BR tags and replace them with line break
    returnText = returnText.replace(/<br>/gi, " ");
    returnText = returnText.replace(/<br\s\/>/gi, " ");
    returnText = returnText.replace(/<br\/>/gi, " ");

    //-- remove P and A tags but preserve what inside of them
    //returnText=returnText.replace(/<p.*>/gi, "\n");    //с ним удаляется всё что в тэге <p>
    returnText = returnText.replace(/<a.*href="(.*?)".*>(.*?)<\/a>/gi, "");

    //-- remove all inside SCRIPT and STYLE tags
    returnText = returnText.replace(/<script.*?>[\w\W]*?<\/script>/gi, "");
    returnText = returnText.replace(/<style.*?>[\w\W]*?<\/style>/gi, "");
    //returnText = returnText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
    //-- remove all else" html> &#10005; Избранное &#10006; История Будь  цифровым О компании  Инжиниринговый дивизион – центр инжиниринговых и проектных компетенций атомной отрасли.  В разделе приведены основные характеристики Дивизиона и входящих в него компаний. 23 число стран, в которых используют проекты 33 энергоблока в портфеле 16 677 общая численность сотрудников Стратегия  Стратегия Дивизиона направлена на удержание лидерских позиций на рынке строительства АЭС и на поддержание операционной и финансовой устойчивости.  В разделе описаны кратко- и долгосрочные стратегические цели, а также вклад отчетного года в их достижение. 92,2 млрд  долларов  США портфель зарубежных заказов 91,8 млрд руб. портфель заказов по новым продуктам 2 712,7 млн руб. объём инвестиций Бизнес-модель и управление капиталами  В основе бизнес-модели Компании лежит ее долгосрочная стратегия и устойчивое развитие бизнеса.  В разделе схематично показана бизнес-модель, охарактеризованы капиталы и их динамика. 175,2 млрд руб. выручка +12,9% рост производительности труда 17,97 млн руб. расходы на охрану окружающей среды Трансформация Инжинирингового дивизиона в цифровую компанию Создана главная лаборатория цифровой экономики 8,18% доля инновационной продукции в выручке Новые партнеры: SAP SE, IBM, HILTI, Assystem SA и др. Взаимодействие с заинтересованными сторонами  Инжиниринговый дивизион в своей деятельности стремится к формированию партнерских и взаимовыгодных отношений с заинтересованными сторонами.  В разделе перечислены основные принципы взаимодействия с заинтересованными сторонами, приведены результаты взаимодействия в отчетном году. 3 678 количество созданных  рабочих мест 229 млн руб. направлено на развитие регионов  в виде благотворительных средств > 400 млн руб. затраты  на охрану труда Об отчете  Отчет подготовлен на уровне лучших мировых практик годовой отчетности и соответствует требованиям Стандарта интегрированной отчетности и новой редакции GRI Standards.  В разделе дана информация о границах отчетности, указаны существенные темы, перечислены процедуры по верификации Отчета. Публичные отчеты: интегрированный отчет Дивизиона, буклет Дивизиона по итогам года, отчеты дочерних обществ, буклеты по итогам года АЭС «Руппур» и АЭС «Пакш 2». Впервые диалог со стейкхолдерами прошел на площадке строительства – на Нововоронежской АЭС-2 Определение существенности по технологии Rapid Foresight "
    returnText = returnText.replace(/<(?:.|\s)*?>/g, "");

    //-- get rid of more than 2 multiple line breaks:
    returnText = returnText.replace(/(?:(?:\r\n|\r|\n)\s*){2,}/gim, "  ");

    //-- get rid of more than 2 spaces:
    returnText = returnText.replace(/ +(?= )/g, '');

    //-- get rid of html-encoded characters:
    returnText = returnText.replace(/&nbsp;/gi, " ");
    returnText = returnText.replace(/&amp;/gi, "&");
    returnText = returnText.replace(/&quot;/gi, '"');
    returnText = returnText.replace(/&lt;/gi, '<');
    returnText = returnText.replace(/&gt;/gi, '>');

    //-- return
    return returnText.replace(/\r?\n|\r/g, '');
}