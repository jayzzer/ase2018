const tooltip = document.querySelector('.tooltip');

function showTooltip(e) {
    const targetRect = getOffsetRect(e.target);
    const tooltipText = e.target.dataset.tooltip;

    tooltip.style.visibility = 'visible';

    tooltip.innerHTML = tooltipText;
    
    tooltip.style.top = targetRect.top - 32 - tooltip.offsetHeight/2 + 'px';
    tooltip.style.left = targetRect.left + 'px';

    tooltip.style.opacity = 1;
}

function hideTooltip() {
    tooltip.style.opacity = 0;
    tooltip.style.visibility = 'hidden';
}