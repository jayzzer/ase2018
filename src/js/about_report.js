const svg = document.querySelector('.matrix-circles');
const circles = document.querySelectorAll('.matrix-circles a');

const tooltip = document.querySelector('.tooltip');

let i = circles.length;
while(i--) {
    const target = circles[i];
    const tooltipText = target.dataset.tooltip;
    target.addEventListener("mouseenter", function(e) {
        svg.removeChild(target);
        svg.appendChild(target);
        
        const targetRect = getOffsetRect(target);

        tooltip.style.visibility = 'visible';

        tooltip.innerHTML = tooltipText;
        
        tooltip.style.top = targetRect.top - 32 - tooltip.offsetHeight/2 + 'px';
        tooltip.style.left = targetRect.left + 'px';

        tooltip.style.opacity = 1;
    });

    target.addEventListener("mouseleave", function(e) {
        tooltip.style.opacity = 0;
        tooltip.style.visibility = 'hidden';
    });
}


function getOffsetRect(elem) {
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docElem = document.documentElement;

    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

    var clientTop = docElem.clientTop || body.clientTop || 0;
    var clientLeft = docElem.clientLeft || body.clientLeft || 0;

    var top  = box.top +  scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;
    
    return { top: Math.round(top), left: Math.round(left) };
}