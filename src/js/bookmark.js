document.addEventListener('DOMContentLoaded', () => {
    updateBookmarksList();

    const bookmarksEl = document.getElementById('bookmarks-list');
    const historyEl = document.getElementById('history-list');

    let sidepanelFsp = fsp('#open-bookmark', '#main', '#right-sidepanel').sidepanel({
        side: 'right'
    });

    document.getElementById('close-sidepanel').addEventListener('click', () => {
        sidepanelFsp.toggle();
    });

    document.getElementById('delete-bookmarks').addEventListener('click', () => {
        let bookmarksListEl = document.getElementById('bookmarks-list');
        while (bookmarksListEl.firstElementChild) {
            const bookmarkEl = bookmarksListEl.firstElementChild;

            deleteBookmark(bookmarkEl.querySelector('.bookmark-title').innerHTML);
            bookmarksListEl.removeChild(bookmarkEl);
        }
    });

    document.getElementById('open-history').addEventListener('click', () => {
        bookmarksEl.classList.remove('show');
        historyEl.classList.remove('show');

        bookmarksEl.previousElementSibling.previousElementSibling.classList.add('collapsed');
        historyEl.previousElementSibling.classList.add('collapsed');

        historyEl.classList.add('show');
        historyEl.previousElementSibling.classList.remove('collapsed');

        sidepanelFsp.toggle();
    });

    document.getElementById('open-bookmark').addEventListener('click', () => {
        bookmarksEl.classList.remove('show');
        historyEl.classList.remove('show');

        bookmarksEl.previousElementSibling.previousElementSibling.classList.add('collapsed');
        historyEl.previousElementSibling.classList.add('collapsed');
        
        bookmarksEl.classList.add('show');
        bookmarksEl.previousElementSibling.previousElementSibling.classList.remove('collapsed');
    });
});

function updateBookmarksList() {
    let bookmarksListEl = document.getElementById('bookmarks-list');
    let bookmarks = getCookie('bookmarks');

    if (bookmarks) {
        bookmarks = JSON.parse(bookmarks);
    } else {
        bookmarks = []
    }

    let bookmarksList = '';

    for (let i = bookmarks.length - 1; i >= 0; i--) {
        bookmarksList += `
            <li>
                <span class="delete-bookmark mr-1">&#10005;</span><a class="bookmark-title" href="${bookmarks[i].url}">${bookmarks[i].title}</a>
            </li>
        `
    };

    bookmarksListEl.innerHTML = bookmarksList;

    bookmarksListEl.addEventListener('click', (e) => {
        let target = e.target;
        if (target.classList.contains('delete-bookmark')) {
            const title = target.nextElementSibling.innerHTML;
            deleteBookmark(title);

            target.parentNode.remove();
        }
    })
}

function addBookmark() {
    const bookmark = {
        url: document.location.href,
        title: document.title
    };
    
    let newBookmarks = getCookie('bookmarks');
    if (newBookmarks) {
        newBookmarks = JSON.parse(newBookmarks);
    } else {
        newBookmarks = [];
    }

    for (let i = 0; i < newBookmarks.length; i++) {
        if (newBookmarks[i].title == bookmark.title) 
            return;
    }

    newBookmarks.push(bookmark);
    setCookie('bookmarks', JSON.stringify(newBookmarks));

    updateBookmarksList();
}

function deleteBookmark(bookmarkTitle) {
    let bookmarks = getCookie('bookmarks');
    if (bookmarks) {
        bookmarks = JSON.parse(bookmarks);
    } else {
        bookmarks = []
    }

    for (let i = 0; i < bookmarks.length; i++) {
        if (bookmarks[i].title == bookmarkTitle) {
            bookmarks.splice(i, 1);

            setCookie('bookmarks', JSON.stringify(bookmarks));

            return;
        }
    }
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {};
  
    var expires = options.expires;
  
    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }
  
    value = encodeURIComponent(value);
  
    var updatedCookie = name + "=" + value;
  
    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
  
    document.cookie = updatedCookie;
}