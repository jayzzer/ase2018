let ChartA = echarts.init(document.getElementById('ChartA'));

optionA = {
    tooltip: {
        trigger: 'axis',
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color : ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    grid: {
        left: '10%',
        right: '5%',
        bottom: '5%',
        top: '15%',
        containLabel: true
    },
    xAxis: {
        type: 'category',
        data: ['2015', '2016', '2017']
    },
    yAxis: {
        name: 'шт',
        type: 'value'
    },
    series: [{
        data: [37, 43, 40],
        type: 'line'
    }]
};
ChartA.setOption(optionA);

let ChartB = echarts.init(document.getElementById('ChartB'));
optionB = {
    tooltip: {
        trigger: 'item',
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color : ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        y: 'bottom',
        data: ['Всего корректирующих мероприятий к исполнению', 'Мероприятия срок исполнения по которым наступил', 'Исполнено мероприятий']
    },
    calculable: true,
    grid: {
        left: '15%',
        top: '15%',
        bottom: '30%'
    },
    xAxis: [
        {
            type: 'category',
            data: ['2016', '2017']
        }
    ],
    yAxis: [
        {
            name: `Количество мероприятий`,
            type: 'value'
        }
    ],
    series: [
        {
            name: 'Всего корректирующих мероприятий к исполнению',
            type: 'bar',
            data: [98, 145]
        },
        {
            name: 'Мероприятия срок исполнения по которым наступил',
            type: 'bar',
            data: [98, 121]
        },
        {
            name: 'Исполнено мероприятий',
            type: 'bar',
            data: [95, 93]
        }
    ]
};
ChartB.setOption(optionB);
