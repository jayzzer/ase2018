let ChartC = echarts.init(document.getElementById('ChartC'));

option = {
    tooltip: {
        trigger: 'axis',
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    legend: {
        data: ['Текущий уровень, %', 'Целевой уровень, %']
    },
    color : ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    grid: {
        left: '3%',
        right: '4.7%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: [
        {
            type: 'category',
            boundaryGap: false,
            data: ['Elementaty', 'Pre-Intermediate', 'Intermediate', 'Upper-Intermediate', 'Advanced']
        }
    ],
    yAxis: [

        {
            type: 'value',
            axisLabel: {
                formatter: '{value}%'
            }
        }
    ],
    series: [
        {
            name: 'Текущий уровень, %',
            type: 'line',
            stack: '1',
            label: {
                normal: {
                    formatter: '{c}%',
                    show: true,
                    position: 'bottom',
                }
            },
            areaStyle: { normal: {} },
            data: [23, 23, 15, 3, 1]
        },
        {
            name: 'Целевой уровень, %',
            type: 'line',
            stack: '1',
            label: {
                normal: {
                    formatter: '{c}%',
                    show: true,
                    position: 'top',
                }
            },
            areaStyle: { normal: {} },
            data: [0, 12, 79, 6, 2]
        }
    ]
};

ChartC.setOption(option);
