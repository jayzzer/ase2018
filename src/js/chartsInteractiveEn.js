let Chart = echarts.init(document.getElementById('ChartA'));

optionsA = {
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    tooltip: {
        trigger: 'axis',
        position: function (pos, params, dom, rect, size) {
            // tooltip will be fixed on the right if mouse hovering on the left,
            // and on the left if hovering on the right.
            var obj = {top: 60};
            obj[['left', 'right'][+(pos[0] < size.viewSize[0] / 2)]] = 5;
            return obj;
        },
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['2015','2016','2017 plan','2017 fact','2018 plan']
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name:'10-year Portfolio of Overseas Orders (traditional products and diversification) (∆ 2017/2016 (%): -0.2)',
            type:'line',
            stack: '10-year Portfolio of Overseas Orders (traditional products and diversification), bin USD',
            data:[70.1, 92.3, 102.2, 92.2, 88.7]
        }
    ]
}

Chart.setOption(optionsA);

let app, optionsB;

optionsB = {
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        },
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    legend: {
        data: ['Forest', 'Steppe', 'Desert', 'Wetland']
    },
    toolbox: {
        show: true,
        orient: 'vertical',
        left: 'right',
        top: 'center'
    },
    calculable: true,
    xAxis: [
        {
            type: 'category',
            axisTick: {show: false},
            data: ['2015','2016','2017 planned','2017 fact','2018 planned']
        }
    ],
    yAxis: [
        {
            type: 'value'
        }
    ],
    series: [
        {
            name:'10-year Portfolio of Overseas Orders (traditional products and diversification), bin USD (∆ 2017/2016 (%): -0.2)',
            type:'bar',
            barGap: 0,
            data:[70.1, 92.3, 102.2, 92.2, 88.7]
        }
    ]
};

function switchToBar(){
    reloadChart(optionsB);
}

function switchToLine(){
    reloadChart(optionsA)
}

function reloadChart(options) {
    document.getElementById("ChartA").remove();
    let as = `<div class="col-lg-12 col-md-12 col-sm-12" id="ChartA" style="height: 400px;"></div>`;
    document.getElementById("charts").innerHTML = as;
    Chart = echarts.init(document.getElementById('ChartA'));
    Chart.setOption(options);
}

$('#inputForm').on('change', ':checkbox', function () {

    if ($(this).is(':checked')) {
        optionsA.series.push(
            {
                name: dataset[Number($(this).val())-1].name,
                type:'line',
                stack: dataset[Number($(this).val())-1].name,
                data: dataset[Number($(this).val())-1].data
            }
        );
        //TODO: optionsB
        optionsB.series.push(
            {
                name: dataset[Number($(this).val())-1].name,
                type:'bar',
                barGap: 0,
                data: dataset[Number($(this).val())-1].data
            }
        );
    } else {
        //TODO: удаление
        for(let i=0; i<optionsA.series.length; i++){
            if(optionsA.series[i].name==dataset[Number($(this).val())-1].name) {
                optionsA.series.splice(i,1);
                break;
            }
        }

        for(let i=0; i<optionsB.series.length; i++){
            if(optionsB.series[i].name==dataset[Number($(this).val())-1].name) {
                optionsB.series.splice(i,1);
                break;
            }
        }
        console.log($(this).val() + ' is now unchecked');
    }

    if (document.getElementById("radio_1").checked){
        reloadChart(optionsA); 
        console.log('обновилл');
    } 
    else {
        reloadChart(optionsB);
        console.log('обновилл');
    }
});

//---------dataset------------

let dataset = [
    {name: '10-year Portfolio of Overseas Orders (traditional products and diversification) (∆ 2017/2016 (%): -0.2)', data: [70.1, 92.3, 102.2, 92.2, 88.7]},
    {name: '10-year Portfolio of New Products* (outside the scope of the Corporation), bin RUR (∆ 2017/2016 (%): 1.2)', data: [59.5, 90.8, 99.1, 91.9, 99.1]},
    {name: 'Revenue, bin RUR (∆ 2017/2016 (%): 14.6)', data: [166.6, 152.9, 186.6, 175.2, 235.7]},
    {name: 'EBITDA, bin RUR (∆ 2017/2016 (%): -8.4)', data: [6.57, 32.4, 11.3, 13.9, 7.7]},
    {name: 'CAPEX, bin RUR (∆ 2017/2016 (%): 85)', data: [1.5, 1.5, 6.7, 2.7, 20.7]},
    {name: 'Labour Productivity (in terms of proper revenue), mln RUR/capita (∆ 2017/2016 (%): 12.9)', data:[3.0, 3.3, 3.7, 3.7, 4.35]},
    {name: 'Number of Power Units in the Portfolio (∆ 2017/2016 (%): 0)', data:[32, 33, 33, 33]},
    {name: 'Expenditures for Environmental Protection Measures, mln RUR (∆ 2017/2016 (%): 18.2)', data:[22.5, 15.2, 8.4, 18.0]},
    {name: 'Electrical Power, GWh (∆ 2017/2016 (%): -19)', data:[46.2, 39.8, , 32.4]},
    {name: 'Thermal Power, TJ (∆ 2017/2016 (%): -22)', data:[204.4, 202, ,158.3]},
    {name: 'Total volume of waste, tons (∆ 2017/2016 (%): -7)', data:[4728.9, 5492.2, , 5109.9]},
    {name: 'Average Staff, pers. (∆ 2017/2016 (%): -3.14)', data:[17961, 14919, 14634, 14450, 17112]},
    {name: 'Total Number of Employees as of December 31 of Each Year (GRI 102-7), pers. (∆ 2017/2016 (%): 14.5)', data:[17755, 14562, 14999, 16677, 18273]},
    {name: 'Number of New Jobs, pcs (∆ 2017/2016 (%): 50.3)', data:[3425, 2447, 1864, 3678, 2698]},
    {name: 'Average Age of Employees, years (∆ 2017/2016 (%): -1)', data:[41.9, 41.3, , 40.8]},
    {name: 'Salary and Other Benefits per one Employee, mln RUR (∆ 2017/2016 (%): 1.3)', data:[1.1, 1.17, 1.25, 1.18, 1.27]} 
]
