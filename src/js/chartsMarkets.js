let myChart = echarts.init(document.getElementById('ChartF'));

optionF = {
    title : [{
        text: 'Общий портфель заказов \nна сооружение АЭС, % (э/б)',
        x:130
    },
    {
        text: 'Портфель зарубежных заказов \nна сооружение АЭС, % (э/б)',
        x:630
    }
    ],
    color: ['#21348C','#3CD52E','#009EE3','#7B1C81','#C5C6C6', '#484847'],
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {d}%",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    legend: {
        x : 'center',
        y : 'bottom',
        data:['Госкорпорация «Росатом»*', 
            'Китай (China National Nuclear Corp, China General Nuclear Power Group)', 
            'KEPCO', 
            'AREVA', 
            'Toshiba Westinghouse', 
            'Прочие']
    },
    toolbox: {
        show : true,
        feature : {
            mark : {show: true},
            magicType : {
                show: true,
                type: 'pie'
            }
        }
    },
    calculable : true,
    series : [
        {
            type:'pie',
            radius : '50%',
            center : ['25%', '50%'],
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
            data:[
                {value: 41, name: 'Госкорпорация «Росатом»*'},
                {value: 13, name: 'Китай (China National Nuclear Corp, China General Nuclear Power Group)'},
                {value: 11, name: 'KEPCO'},
                {value: 7, name: 'AREVA'},
                {value: 8, name: 'Toshiba Westinghouse'},
                {value: 6, name: 'Прочие'}
            ]
        },
        {
            type:'pie',
            radius : '50%',
            center : ['75%', '50%'],
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
            data:[
                {value: 33, name: 'Госкорпорация «Росатом»*'},
                {value: 2, name: 'Китай (China National Nuclear Corp, China General Nuclear Power Group)'},
                {value: 7, name: 'KEPCO'},
                {value: 6, name: 'AREVA'},
                {value: 4, name: 'Toshiba Westinghouse'},
                {value: 6, name: 'Прочие'}
            ]
        }
    ]
};


myChart.setOption(optionF);