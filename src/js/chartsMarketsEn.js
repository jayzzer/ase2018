let myChart = echarts.init(document.getElementById('ChartFEn'));

optionF = {
    title : [{
        text: 'The total portfolio of orders \nfor NPP construction, % (PU)',
        x:130
    },
    {
        text: 'The portfolio of foreign orders \nfor NPP construction, % (PU)',
        x:640
    }
    ],
    color: ['#21348C','#3CD52E','#009EE3','#7B1C81','#C5C6C6', '#484847'],
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {d}%",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    legend: {
        x : 'center',
        y : 'bottom',
        data:['Rosatom State Corporation*', 
            'China (China National Nuclear Corp, China General Nuclear Power Group)', 
            'KEPCO', 
            'AREVA', 
            'Toshiba Westinghouse', 
            'Other']
    },
    toolbox: {
        show : true,
        feature : {
            mark : {show: true},
            magicType : {
                show: true,
                type: 'pie'
            }
        }
    },
    calculable : true,
    series : [
        {
            type:'pie',
            radius : '50%',
            center : ['25%', '50%'],
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
            data:[
                {value: 41, name: 'Rosatom State Corporation*'},
                {value: 13, name: 'China (China National Nuclear Corp, China General Nuclear Power Group)'},
                {value: 11, name: 'KEPCO'},
                {value: 7, name: 'AREVA'},
                {value: 8, name: 'Toshiba Westinghouse'},
                {value: 6, name: 'Other'}
            ]
        },
        {
            type:'pie',
            radius : '50%',
            center : ['75%', '50%'],
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
            data:[
                {value: 33, name: 'Rosatom State Corporation*'},
                {value: 2, name: 'China (China National Nuclear Corp, China General Nuclear Power Group)'},
                {value: 7, name: 'KEPCO'},
                {value: 6, name: 'AREVA'},
                {value: 4, name: 'Toshiba Westinghouse'},
                {value: 6, name: 'Other'}
            ]
        }
    ]
};


myChart.setOption(optionF);