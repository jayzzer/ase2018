let ChartD = echarts.init(document.getElementById('ChartD'));

optionD = {
    title: {
        text: 'Благотворительные проекты, реализуемые на \n территории Российской Федерации',
        x: 'center'
    },
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} млн руб. ({d}%)",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color : ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        x: 'center',
        bottom: 0,
        data: ['Владимирская область', 'Воронежская область', 'Калининградская область', 'Курская область', 'Нижегородская область', 'Саратовская\nобласть', 'Смоленская\nобласть', 'Ростовская\nобласть', 'г.Москва', 'г.Санкт-Петербург', 'Республика Беларусь']
    },
    series: [
        {
            name: '2017 год',
            type: 'pie',
            radius: '55%',
            center: ['50%', '45%'],
            data: [
                {value: 0.3, name: 'Владимирская область'},
                {value: 5.2, name: 'Воронежская область'},
                {value: 0, name: 'Калининградская область'},
                {value: 9.7, name: 'Курская область'},
                {value: 147.93, name: 'Нижегородская область'},
                {value: 0.4, name: 'Саратовская\nобласть'},
                {value: 0.8, name: 'Смоленская\nобласть'},
                {value: 23.9, name: 'Ростовская\nобласть'},
                {value: 57.1, name: 'г.Москва'},
                {value: 2.4, name: 'г.Санкт-Петербург'},
                {value: 7.4, name: 'Республика Беларусь'}
            ],
            label: {
                normal: {
                    show: false
                }
            },
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
        }
    ]
};

ChartD.setOption(optionD);

let ChartE = echarts.init(document.getElementById('ChartE'));

optionE = {
    title: {
        text: 'Благотворительные проекты, \n реализуемые за рубежом',
        x: 'center'
    },
    color : ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} млн руб. ({d}%)",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    legend: {
        x: 'center',
        bottom: 30,
        data: ['Исламская Республика Иран', '\nКитайская Народная\nРеспублика', 'Народная\nРеспублика\nБангладеш', 'Республика\nКазахстан', 'Социалистическая\nРеспублика\nВьетнам']
    },
    series: [
        {
            name: '2017 год',
            type: 'pie',
            radius: '55%',
            center: ['50%', '45%'],
            data: [
                {value: 0.5, name: 'Исламская Республика Иран'},
                {value: 0.5, name: '\nКитайская Народная\nРеспублика'},
                {value: 9, name: 'Народная\nРеспублика\nБангладеш'},
                {value: 3, name: 'Республика\nКазахстан'},
                {value: 6, name: 'Социалистическая\nРеспублика\nВьетнам'},
            ],
            label: {
                normal: {
                    show: false
                }
            },
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
        }
    ]
};

ChartE.setOption(optionE);

let ChartF = echarts.init(document.getElementById('ChartF'));

optionF = {
    legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        itemWidth: 13,
        type: 'scroll',
        data: ['Акционер', 'Органы, представляющие\n интересы работников','Персонал','Органы местного самоуправления','Государственные органы контроля','Международные организации','Проф. ассоциации','Органы гос. власти','Страховщики','Финансовые учреждения','Научное сообщество','Менеджмент','Население регионов','СМИ','Образовательные учреждения','Общенственные организации','Заказчики','Поставщики','Поставщики']
    },
    tooltip: {
        trigger: 'item',
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        },
        formatter: '{a}: <br /> {c}'
    },
    xAxis: {
        name: '\nВлияние на компанию',
        nameLocation: 'middle',
        splitLine: {
            lineStyle: {
                type: 'dashed'
            }
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    grid: {
        top:'10%',
        containLabel: true
    },
        yAxis: {
        name: '                           Влияние на стейкхолдеров',
        splitLine: {
            lineStyle: {
                type: 'dashed'
            }
        },
        scale: true
    },
    series: [
        {
        name: 'Акционер',
        data: [[2.0, 1.2]],
        type: 'effectScatter',
        symbolSize: 20,
        showEffectOn: 'render',
        rippleEffect: {
            brushType: 'stroke'
        },
        hoverAnimation: true
    },
        {
            name: 'Органы, представляющие\n интересы работников',
            data: [[0.95, 1.6]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Персонал',
            data: [[0.8, 1.6]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Органы местного самоуправления',
            data: [[1.2, 0.6]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Государственные органы контроля',
            data: [[2.0, 0.6]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Международные организации',
            data: [[1.55, 1.1]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Проф. ассоциации',
            data: [[0.75, 0.8]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Органы гос. власти',
            data: [[1.4, 0.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Страховщики',
            data: [[1.35, 1.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Финансовые учреждения',
            data: [[1.5, 1.2]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Научное сообщество',
            data: [[0.4, 0.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Менеджмент',
            data: [[1.6, 1.2]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Население регионов',
            data: [[0.7, 1.6]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'СМИ',
            data: [[1.55, 0.95]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Образовательные учреждения',
            data: [[1.45, 1.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Общенственные организации',
            data: [[1.0, 0.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Заказчики',
            data: [[1.8, 1.7]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Поставщики',
            data: [[1.0, 1.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }]
};

ChartF.setOption(optionF);
