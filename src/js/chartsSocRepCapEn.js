let ChartD = echarts.init(document.getElementById('ChartDEn'));

optionD = {
    title: {
        text: 'Charity projects implemented \non the territory of the Russian Federation',
        x: 'center'
    },
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} mln RUR ({d}%)",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color : ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        x: 'center',
        bottom: 0,
        data: ['Vladimir region', 
                'Voronezh region', 
                'Kaliningrad region', 
                'Kursk region', 
                'Nizhny Novgorod region', 
                'Saratov \nregion', 
                'Smolensk \nregion', 
                'Rostov \nregion', 
                'Moscow', 
                'St.Petersburg', 
                'Republic of Belarus']
    },
    series: [
        {
            name: '2017 year',
            type: 'pie',
            radius: '55%',
            center: ['50%', '45%'],
            data: [
                {value: 0.3, name: 'Vladimir region'},
                {value: 5.2, name: 'Voronezh region'},
                {value: 0, name: 'Kaliningrad region'},
                {value: 9.7, name: 'Kursk region'},
                {value: 147.93, name: 'Nizhny Novgorod region'},
                {value: 0.4, name: 'Saratov \nregion'},
                {value: 0.8, name: 'Smolensk \nregion'},
                {value: 23.9, name: 'Rostov \nregion'},
                {value: 57.1, name: 'Moscow'},
                {value: 2.4, name: 'St.Petersburg'},
                {value: 7.4, name: 'Republic of Belarus'}
            ],
            label: {
                normal: {
                    show: false
                }
            },
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
        }
    ]
};

ChartD.setOption(optionD);

let ChartE = echarts.init(document.getElementById('ChartEEn'));

optionE = {
    title: {
        text: 'Charity projects \nimplemented abroad',
        x: 'center'
    },
    color : ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} mln RUR ({d}%)",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    legend: {
        x: 'center',
        bottom: 30,
        data: ['Islamic Republic of Iran', 
                '\nPeople’s Republic \nof China', 
                'People’s \nRepublic \nof Bangladesh', 
                'Republic \nof Kazakhstan', 
                'Socialist \nRepublic \nof Vietnam']
    },
    series: [
        {
            name: '2017 year',
            type: 'pie',
            radius: '55%',
            center: ['50%', '45%'],
            data: [
                {value: 0.5, name: 'Islamic Republic of Iran'},
                {value: 0.5, name: '\nPeople’s Republic \nof China'},
                {value: 9, name: 'People’s \nRepublic \nof Bangladesh'},
                {value: 3, name: 'Republic \nof Kazakhstan'},
                {value: 6, name: 'Socialist \nRepublic \nof Vietnam'},
            ],
            label: {
                normal: {
                    show: false
                }
            },
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
        }
    ]
};

ChartE.setOption(optionE);

let ChartF = echarts.init(document.getElementById('ChartFEn'));

optionF = {
    legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        itemWidth: 13,
        type: 'scroll',
        data: ['Shareholder', 
                'Bodies representing \nthe interests of workers',
                'Staff',
                'Local government',
                'State control bodies',
                'International organization',
                'Union organization',
                'Public authority',
                'Insurers',
                'Financial institution',
                'Scientific community',
                'Management',
                'Population of region',
                'MEDIA',
                'Educational institution',
                'Public organization',
                'Customers',
                'Suppliers']
    },
    tooltip: {
        trigger: 'item',
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        },
        formatter: '{a}: <br /> {c}'
    },
    xAxis: {
        name: '\nImpact on the company',
        nameLocation: 'middle',
        splitLine: {
            lineStyle: {
                type: 'dashed'
            }
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    grid: {
        top:'10%',
        containLabel: true
    },
        yAxis: {
        name: '                           Impact on stakeholders',
        splitLine: {
            lineStyle: {
                type: 'dashed'
            }
        },
        scale: true
    },
    series: [{
        name: 'Shareholder',
        data: [[2.0, 1.2]],
        type: 'effectScatter',
        symbolSize: 20,
        showEffectOn: 'render',
        rippleEffect: {
            brushType: 'stroke'
        },
        hoverAnimation: true
    },
        {
            name: 'Bodies representing \nthe interests of workers',
            data: [[0.95, 1.6]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Staff',
            data: [[0.8, 1.6]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Local government',
            data: [[1.2, 0.6]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'State control bodies',
            data: [[2.0, 0.6]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'International organization',
            data: [[1.55, 1.1]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Union organization',
            data: [[0.75, 0.8]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Public authority',
            data: [[1.4, 0.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Insurers',
            data: [[1.35, 1.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Financial institution',
            data: [[1.5, 1.2]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Scientific community',
            data: [[0.4, 0.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Management',
            data: [[1.6, 1.2]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Population of region',
            data: [[0.7, 1.6]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'MEDIA',
            data: [[1.55, 0.95]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Educational institution',
            data: [[1.45, 1.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Public organization',
            data: [[1.0, 0.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Customers',
            data: [[1.8, 1.7]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }, {
            name: 'Suppliers',
            data: [[1.0, 1.4]],
            type: 'effectScatter',
            symbolSize: 20,
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true
        }]
};

ChartF.setOption(optionF);
