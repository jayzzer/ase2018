document.addEventListener('DOMContentLoaded', () => {
    addHistory();
    updateHistoryList();
});

function updateHistoryList() {
    let historyListEl = document.getElementById('history-list');
    let history = getCookie('history');

    if (history) {
        history = JSON.parse(history);
    } else {
        history = [];
    }

    let historyList = '';

    for (let i = history.length - 1; i >= 0; i--) {
        historyList += `
            <li>
                <a class="history-title" href="${history[i].url}">${history[i].title}</a>
            </li>
        `
    };

    historyListEl.innerHTML = historyList;
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {};
  
    var expires = options.expires;
  
    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }
  
    value = encodeURIComponent(value);
  
    var updatedCookie = name + "=" + value;
  
    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
  
    document.cookie = updatedCookie;
}

function addHistory() {
    const history = {
        url: document.location.href,
        title: document.title
    };
    
    let newHistory = getCookie('history');
    
    if (newHistory) {
        newHistory = JSON.parse(newHistory);
    } else {
        newHistory = [];
    }

    for (let i = 0; i < newHistory.length; i++) {
        if (newHistory[i].title == history.title){
            newHistory.splice(i, 1);
        } 
            
    }

    if(newHistory.length > 5) {
        newHistory.shift();
    }
    
    newHistory.push(history);
    setCookie('history', JSON.stringify(newHistory));
}