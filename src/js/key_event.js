const allInd = document.querySelectorAll('.carousel-indicators li');

const currentBtn = document.getElementById('current-btn');
const otherBtn = document.getElementById('other-btn');

const currentEvents = document.querySelectorAll('.carousel-indicators .current-ev');
const otherEvents = document.querySelectorAll('.carousel-indicators .other-ev');

let active = 'none';

currentBtn.addEventListener( 'click', () => {
    currentBtn.classList.toggle('active');
    
    switch(active) {
        case 'current':
            resetInd();
            active = 'none';

            break;
        case 'other':
            resetInd();
            active = 'current';
            otherBtn.classList.remove('active');

            currentEvents.forEach( event => {
                event.style.backgroundColor = '#792A91';
            } );

            break;
        case 'none':
            active = 'current';

            currentEvents.forEach( event => {
                event.style.backgroundColor = '#792A91';
            } );

            break;
    }
} );

otherBtn.addEventListener( 'click', () => {
    otherBtn.classList.toggle('active');

    switch(active) {
        case 'current':
            resetInd();
            active = 'other';

            currentBtn.classList.remove('active');

            otherEvents.forEach( event => {
                event.style.backgroundColor = '#54D335';
            } );

            break;
        case 'other':
            resetInd();
            active = 'none';

            break;
        case 'none':
            active = 'other';

            otherEvents.forEach( event => {
                event.style.backgroundColor = '#54D335';
            } );

            break;
    }
} );


function resetInd() {
    allInd.forEach( ind => {
        ind.style = '';
    } );
}