let myChart = echarts.init(document.getElementById('lineProdEn'));

option = {
    tooltip : {
        trigger: 'axis',
        formatter: "{b} year : {c} mln RUR",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    xAxis: {
        type: 'category',
        data: ['2015', '2016', '2017']
    },
    yAxis: {
        type: 'value'
    },
    series: [{
        data: [7943.33, 14737.96, 19099.42],
        type: 'line',
        color: '#4A7EBB'
    }]
};

myChart.setOption(option);