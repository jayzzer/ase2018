const nameMap = {
    'Austria': 'Австрия <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'Russia': 'Россия <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png">  <span class="block"><img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">     <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сервис" src="/img/map/light_blue_circle.png">   <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Консультационные услуги по управлению проектом" src="/img/map/purple_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Филиалы" src="/img/map/blue_ring.png">   <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Представительства" src="/img/map/green_ring.png">',
    'Armenia': 'Армения <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'Bangladesh': 'Бангладеш <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Филиалы" src="/img/map/blue_ring.png">',
    'Belarus': 'Белоруссия <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Представительства" src="/img/map/green_ring.png">',
    'Belgium': 'Бельгия <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'Bulgaria': 'Болгария <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'United Kingdom': 'Великобритания <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'Hungary': 'Венгрия <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Филиалы" src="/img/map/blue_ring.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Представительства" src="/img/map/green_ring.png">',
    'Vietnam': 'Вьетнам <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Представительства" src="/img/map/green_ring.png">',
    'Germany': 'Германия <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'Egypt': 'Египет <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Филиалы" src="/img/map/blue_ring.png">',
    'Jordan': 'Иордания <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png">',
    'India': 'Индия <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Консультационные услуги по управлению проектом" src="/img/map/purple_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Представительства" src="/img/map/green_ring.png">',
    'Iran': 'Иран <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сервис" src="/img/map/light_blue_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Представительства" src="/img/map/green_ring.png">',
    'Iraq': 'Ирак <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'China': 'Китай <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Консультационные услуги по управлению проектом" src="/img/map/purple_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Представительства" src="/img/map/green_ring.png">',
    'Lithuania': 'Литва <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'United Arab Emirates': 'ОАЭ <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'Slovakia': 'Словакия <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Представительства" src="/img/map/green_ring.png">',
    'Turkey': 'Турция <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Консультационные услуги по управлению проектом" src="/img/map/purple_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Филиалы" src="/img/map/blue_ring.png">',
    'Finland': 'Финляндия <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Сооружение АЭС" src="/img/map/blue_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Филиалы" src="/img/map/blue_ring.png">',
    'France': 'Франция <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'Switzerland': 'Швейцария <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
    'Japan': 'Япония <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Вывод из эксплуатации ядерно- и радиационно- опасных объектов, объекты обращения с РАО и ОЯТ">',
};

const selectedCountries = document.querySelectorAll('path.selected');
const countryTooltip = document.getElementById('country-tooltip');

selectedCountries.forEach( country => {
    country.addEventListener( 'click', (e) => {
        showCountryTooltip(e);
    } );
} );

document.querySelector('rect').addEventListener('click', () => {
    hideCountryTooltip();
})


function showCountryTooltip(e) {
    const targetRect = getOffsetRect(e.target);
    const tooltipText = nameMap[e.target.dataset.name];

    countryTooltip.style.visibility = 'visible';

    countryTooltip.innerHTML = tooltipText;
    
    countryTooltip.style.top = targetRect.top - 32 - countryTooltip.offsetHeight/2 + 'px';
    countryTooltip.style.left = targetRect.left + 'px';

    countryTooltip.style.opacity = 1;
}

function hideCountryTooltip() {
    countryTooltip.style.opacity = 0;
    countryTooltip.style.visibility = 'hidden';
}

function getOffsetRect(elem) {
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docElem = document.documentElement;

    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

    var clientTop = docElem.clientTop || body.clientTop || 0;
    var clientLeft = docElem.clientLeft || body.clientLeft || 0;

    var top  = box.top +  scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;
    
    return { top: Math.round(top), left: Math.round(left) };
}
