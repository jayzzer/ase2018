const nameMap = {
    'Austria': 'Austria <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'Russia': 'Russia <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png">  <span class="block"><img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">     <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Maintenance Services" src="/img/map/light_blue_circle.png">   <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Project Management Consulting (PMC) Services" src="/img/map/purple_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Branch office" src="/img/map/blue_ring.png">   <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Representative office" src="/img/map/green_ring.png">',
    'Armenia': 'Armenia <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'Bangladesh': 'Bangladesh <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Branch office" src="/img/map/blue_ring.png">',
    'Belarus': 'Belarus <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Representative office" src="/img/map/green_ring.png">',
    'Belgium': 'Belgium <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'Bulgaria': 'Bulgaria <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'United Kingdom': 'United Kingdom <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'Hungary': 'Hungary <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Branch office" src="/img/map/blue_ring.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Representative office" src="/img/map/green_ring.png">',
    'Vietnam': 'Vietnam <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Representative office" src="/img/map/green_ring.png">',
    'Germany': 'Germany <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'Egypt': 'Egypt <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Branch office" src="/img/map/blue_ring.png">',
    'Jordan': 'Jordan <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png">',
    'India': 'India <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Project Management Consulting (PMC) Services" src="/img/map/purple_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Representative office" src="/img/map/green_ring.png">',
    'Iran': 'Iran <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Maintenance Services" src="/img/map/light_blue_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Representative office" src="/img/map/green_ring.png">',
    'Iraq': 'Iraq <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'China': 'China <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png"> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Project Management Consulting (PMC) Services" src="/img/map/purple_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Representative office" src="/img/map/green_ring.png">',
    'Lithuania': 'Lithuania <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'United Arab Emirates': 'United Arab Emirates <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'Slovakia': 'Slovakia <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Representative office" src="/img/map/green_ring.png">',
    'Turkey': 'Turkey <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Project Management Consulting (PMC) Services" src="/img/map/purple_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Branch office" src="/img/map/blue_ring.png">',
    'Finland': 'Finland <br/> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Core Business. Design and construction of nuclear power plants" src="/img/map/blue_circle.png"> <img onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Branch office" src="/img/map/blue_ring.png">',
    'France': 'France <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'Switzerland': 'Switzerland <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
    'Japan': 'Japan <br/> <img src="/img/map/green_circle.png" onmouseenter="showTooltip(event)" onmouseleave="hideTooltip()" data-tooltip="Construction and upgrading of radioactive waste management and spent nuclear fuel facilities, decommissioning of nuclear radiation facilities">',
};

const selectedCountries = document.querySelectorAll('path.selected');
const countryTooltip = document.getElementById('country-tooltip');

selectedCountries.forEach( country => {
    country.addEventListener( 'click', (e) => {
        showCountryTooltip(e);
    } );
} );

document.querySelector('rect').addEventListener('click', () => {
    hideCountryTooltip();
})


function showCountryTooltip(e) {
    const targetRect = getOffsetRect(e.target);
    const tooltipText = nameMap[e.target.dataset.name];

    countryTooltip.style.visibility = 'visible';

    countryTooltip.innerHTML = tooltipText;
    
    countryTooltip.style.top = targetRect.top - 32 - countryTooltip.offsetHeight/2 + 'px';
    countryTooltip.style.left = targetRect.left + 'px';

    countryTooltip.style.opacity = 1;
}

function hideCountryTooltip() {
    countryTooltip.style.opacity = 0;
    countryTooltip.style.visibility = 'hidden';
}

function getOffsetRect(elem) {
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docElem = document.documentElement;

    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

    var clientTop = docElem.clientTop || body.clientTop || 0;
    var clientLeft = docElem.clientLeft || body.clientLeft || 0;

    var top  = box.top +  scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;
    
    return { top: Math.round(top), left: Math.round(left) };
}
