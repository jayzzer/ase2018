function makePDF() {
    $('html, body').animate( { scrollTop: 0 }, 0);

    let quotes = document.getElementsByClassName('pdf');

    html2canvas(quotes, {
    onrendered:function(canvas) {
  
        let contentWidth = canvas.width,
            contentHeight = canvas.height,
  
        //The height of the canvas which one pdf page can show;
            pageHeight = contentWidth / 592.28 * 841.89,
        //the height of canvas that haven't render to pdf
            leftHeight = contentHeight,
        //addImage y-axial offset
            position = -42,
        //a4 format [595.28,841.89]	      
            imgWidth = 595.28,
            imgHeight = 592.28/contentWidth * contentHeight,
  
            pageData = canvas.toDataURL('image/png', 1.0),
  
            pdf = new jsPDF('', 'pt', 'a4');
  
       if (leftHeight < pageHeight) {
            pdf.addImage(pageData, 'PNG', 0, 0, imgWidth, imgHeight );
        } else {
            while(leftHeight > 0) {
                pdf.addImage(pageData, 'PNG', 0, position, imgWidth, imgHeight)
                leftHeight -= pageHeight;
                position -= 841.89;
                //avoid blank page
                if(leftHeight > 0) {
                    pdf.addPage();
                }
            }
        }
  
        pdf.save('content.pdf');
    }
    })
}