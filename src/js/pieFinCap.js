let myChart = echarts.init(document.getElementById('pieFinCap'));

option = {
    title : [{
        text: 'Основные направления инвестиций',
        x:'center'
    },
    {
        text: '2017 (факт)',
        x:'20%',
        y:'10%'
    },
    {
        text: '2018 (план)',
        x:'70%',
        y:'10%'
    },
    {
        text: '2 713 млн руб.',
        x:'18%',
        y:'17%'
    },
    {
        text: '20 737 млн руб.',
        x:'68%',
        y:'17%'
    }
    ],
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {d}%",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    legend: {
        x : 'center',
        y : 'bottom',
        data:['Проекты оснащения строительных площадок',
            'ИТ-проекты',
            'Проекты по наращиванию компетенций (сделки с акционерным капиталом)',
            'Проекты развития инфраструктуры (в т.ч. обеспечение безопасности)',
            'Прочие (НИОКР, оборудование для инженерно-изыскательных работ)']
    },
    toolbox: {
        show : true,
        feature : {
            mark : {show: true},
            magicType : {
                show: true,
                type: 'pie'
            }
        }
    },
    calculable : true,
    series : [
        {
            name:'2017 год (факт)',
            type:'pie',
            radius : '50%',
            center : ['25%', '50%'],
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
            data:[
                    {value:43, name:'Проекты оснащения строительных площадок'},
                    {value:45, name:'ИТ-проекты'},
                    {value:0, name:'Проекты по наращиванию компетенций (сделки с акционерным капиталом)'},
                    {value:9, name:'Проекты развития инфраструктуры (в т.ч. обеспечение безопасности)'},
                    {value:3, name:'Прочие (НИОКР, оборудование для инженерно-изыскательных работ)'}
            ]
        },
        {
            name:'2018 год (план)',
            type:'pie',
            radius : '50%',
            center : ['75%', '50%'],
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
            data:[
                    {value:81, name:'Проекты оснащения строительных площадок'},
                    {value:12, name:'ИТ-проекты'},
                    {value:3, name:'Проекты по наращиванию компетенций (сделки с акционерным капиталом)'},
                    {value:2, name:'Проекты развития инфраструктуры (в т.ч. обеспечение безопасности)'},
                    {value:2, name:'Прочие (НИОКР, оборудование для инженерно-изыскательных работ)'}
            ]
        }
    ]
};


myChart.setOption(option);