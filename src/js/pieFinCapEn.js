let myChart = echarts.init(document.getElementById('pieFinCapEn'));

option = {
    title : [{
        text: 'CAPEX breakdown',
        x:'center'
    },
    {
        text: '2017 (fact)',
        x:200,
        y:40
    },
    {
        text: '2018 (plan)',
        x:720,
        y:40
    },
    {
        text: '2 713 mln RUR',
        x:190,
        y:65
    },
    {
        text: '20 737 mln RUR',
        x:700,
        y:65
    }
    ],
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {d}%",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    legend: {
        x : 'center',
        y : 'bottom',
        data:['Projects for construction sites equipment',
            'IT-projects',
            'Competency building projects (stockholders’ equity transactions)',
            'Infrastructure development projects (i.e. safety assurance)',
            'Other (R&D, equipment for engineering and survey works)']
    },
    toolbox: {
        show : true,
        feature : {
            mark : {show: true},
            magicType : {
                show: true,
                type: 'pie'
            }
        }
    },
    calculable : true,
    series : [
        {
            name:'2017 year (fact)',
            type:'pie',
            radius : '50%',
            center : ['25%', '50%'],
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
            data:[
                    {value:43, name:'Projects for construction sites equipment'},
                    {value:45, name:'IT-projects'},
                    {value:0, name:'Competency building projects (stockholders’ equity transactions)'},
                    {value:9, name:'Infrastructure development projects (i.e. safety assurance)'},
                    {value:3, name:'Other (R&D, equipment for engineering and survey works)'}
            ]
        },
        {
            name:'2018 year (planned)',
            type:'pie',
            radius : '50%',
            center : ['75%', '50%'],
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
            data:[
                    {value:81, name:'Projects for construction sites equipment'},
                    {value:12, name:'IT-projects'},
                    {value:3, name:'Competency building projects (stockholders’ equity transactions)'},
                    {value:2, name:'Infrastructure development projects (i.e. safety assurance)'},
                    {value:2, name:'Other (R&D, equipment for engineering and survey works)'}
            ]
        }
    ]
};


myChart.setOption(option);