let myChartPie = echarts.init(document.getElementById('pieNatCap'));

option = {
    title : {
        text: 'Проверки состояния промышленной безопасности на сооружаемых объестах в 2017 году',
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {c}",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        bottom: 10,
        left: 'center',
        padding: -10,
        data: ['Нововоронежская АЭС-2',
                'Ростовская АЭС',
                'Курская АЭС-2',
                'Белорусская АЭС']
    },
    series : [
        {
            name: '2017 год',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:2, name:'Нововоронежская АЭС-2'},
                {value:1, name:'Ростовская АЭС'},
                {value:1, name:'Курская АЭС-2'},
                {value:1, name:'Белорусская АЭС'},
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            }
        }
    ]
};

myChartPie.setOption(option);

let myChartPie2 = echarts.init(document.getElementById('pieNatCap2'));

option2 = {
    title : {
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {d}%",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        bottom: 0,
        left: 'center',
        data: ['Практически неопасные отходы (V класса опасности)',
                'Малоопасные отходы(IV класс опасности)',
                'Опасне отходы(I-III классы опасности)']
    },
    series : [
        {
            name: 'Оптимизационные предложения',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:41, name:'Практически неопасные отходы (V класса опасности)'},
                {value:58, name:'Малоопасные отходы(IV класс опасности)'},
                {value:1, name:'Опасне отходы(I-III классы опасности)'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            }
        }
    ]
};

myChartPie2.setOption(option2);