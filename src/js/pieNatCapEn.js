let myChartPie = echarts.init(document.getElementById('pieNatCEn'));

option = {
    title : {
        text: 'Inspections of the industrial safety status at constructed facilities in 2017',
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {c}",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        bottom: 10,
        left: 'center',
        padding: -10,
        data: ['Novovoronezh NPP-2',
                'Rostov NPP',
                'Kursk NPP',
                'Belarus NPP']
    },
    series : [
        {
            name: '2017 year',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:2, name:'Novovoronezh NPP-2'},
                {value:1, name:'Rostov NPP'},
                {value:1, name:'Kursk NPP'},
                {value:1, name:'Belarus NPP'},
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            }
        }
    ]
};

myChartPie.setOption(option);

let myChartPie2 = echarts.init(document.getElementById('pieNatCEn2'));

option2 = {
    title : {
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {d}%",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        bottom: 0,
        left: 'center',
        data: ['Practically non-hazardous waste (hazard class V)',
                'Low hazardous waste (hazard class IV)',
                'Hazardous waste (hazard class I–III)']
    },
    series : [
        {
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:41, name:'Practically non-hazardous waste (hazard class V)'},
                {value:58, name:'Low hazardous waste (hazard class IV)'},
                {value:1, name:'Hazardous waste (hazard class I–III)'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            }
        }
    ]
};

myChartPie2.setOption(option2);