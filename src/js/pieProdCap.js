let myChartPie = echarts.init(document.getElementById('pieProdCap'));

option = {
    title : {
        text: 'Заключенные договорные обязательства в 2017 году',
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {d}%",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        bottom: 10,
        left: 'center',
        padding: -10,
        data: ['АО "НИКИМТ - Атомстрой"',
                'ПАО "Энергоспецмонтаж"',
                'АО "ИК "АСЭ"',
                'АО АСЭ',
                'АО "Атомэнергопроект"',
                'АО "АТОМПРОЕКТ"']
    },
    series : [
        {
            name: '2017 год',
            type: 'pie',
            radius : '55%',
            data:[
                {value:1, name:'АО "НИКИМТ - Атомстрой"'},
                {value:1, name:'ПАО "Энергоспецмонтаж"'},
                {value:32, name:'АО "ИК "АСЭ"'},
                {value:61, name:'АО АСЭ'},
                {value:3, name:'АО "Атомэнергопроект"'},
                {value:2, name:'АО "АТОМПРОЕКТ"'}
            ],
            label: {
                normal: {
                    show: false
                }
            },
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            hoverOffset: 20
        }
    ]
};

myChartPie.setOption(option);

let myChartPie2 = echarts.init(document.getElementById('pieProdCap2'));

option2 = {
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {c}",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        bottom: 0,
        left: 'center',
        data: ['Реализовано в проекте',
                'Реализовано в рабочей документации',
                'В реализации']
    },
    series : [
        {
            name: 'Оптимизационные предложения',
            type: 'pie',
            radius : '70%',
            data:[
                {value:21, name:'Реализовано в проекте'},
                {value:21, name:'Реализовано в рабочей документации'},
                {value:25, name:'В реализации'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
        }
    ]
};

myChartPie2.setOption(option2);