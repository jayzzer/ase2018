let myChartPie = echarts.init(document.getElementById('pieProdEn'));

option = {
    title : {
        text: 'Obligations under concluded contracts in 2017 ',
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {d}%",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        bottom: 10,
        left: 'center',
        padding: -10,
        data: ['JSC NIKIMT-Atomstroy',
                'PJSC Energospezmontazh',
                'JSC ASE EC',
                'JSC ASE',
                'JSC Atomenergoproject',
                'JSC ATOMPROEKT']
    },
    series : [
        {
            name: '2017 year',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:1, name:'JSC NIKIMT-Atomstroy'},
                {value:1, name:'PJSC Energospezmontazh'},
                {value:32, name:'JSC ASE EC'},
                {value:61, name:'JSC ASE'},
                {value:3, name:'JSC Atomenergoproject'},
                {value:2, name:'JSC ATOMPROEKT'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            hoverOffset: 20
        }
    ]
};

myChartPie.setOption(option);

let myChartPie2 = echarts.init(document.getElementById('pieProdCap2En'));

option2 = {
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {c}",
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    legend: {
        bottom: 0,
        left: 'center',
        data: [' Implemented in the project',
                'Implemented in detail design documentation',
                'In implementation phase']
    },
    series : [
        {
            name: 'Optimization proposals',
            type: 'pie',
            radius : '70%',
            center: ['50%', '60%'],
            data:[
                {value:21, name:'Implemented in the project'},
                {value:25, name:'Implemented in detail design documentation'},
                {value:21, name:'In implementation phase'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: false
                }
            },
            lableLine: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
        }
    ]
};

myChartPie2.setOption(option2);