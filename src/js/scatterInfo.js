let lable = ['rsgd','wefe', 'erg'];

let myChart = echarts.init(document.getElementById('Scatter'));

option = {
    tooltip: {
        formatter: param => {
            return param.lable[0];
        },
        backgroundColor: 'rgba(211, 233, 246, 0.9)',
        textStyle: {
            color: '#000'
        }
    },
    color: ['#2B2C6F','#0D4DA1','#09B1EF','#54D335','#792A91', '#00012F', '#333A6D', '#A662BF'],
    xAxis: {
        min: 1,
        max: 5
    },
    yAxis: {
        min: 1,
        max: 4
    },
    series: [{
        symbolSize: 20,
        data: [
            [4.0, 3.2],
            [3.9, 2.8],
            [3.25, 2.1],
            [3.4, 2.75],
            [3.4, 2.57],
            [3.6, 2.9],
            [3.25, 2.65],
            [3.7, 2.9],
            [3.32, 2.7],
            [3.4, 2.9],
            [2.7, 2.59],
            [3.25, 2.85],
            [2.9, 2.1],
            [3.65, 2.75],
            [3.3, 2.55],
            [2.9, 2.45],
            [3.45, 2.6],
            [3.3, 1.8],
            [3.05, 1.7],
            [2.2, 1.6],
            [2.4, 1.7],
            [3.1, 2.5],
            [2.3, 2.0],
            [3.2, 2.9],
            [3.55, 2.8],
            [2.6, 2.45],
            [3.58, 2.6],
            [3.05, 2.2],
            [2.8, 1.8]
        ],
        type: 'scatter'
    }]
};

myChart.setOption(option);